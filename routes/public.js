const express = require("express");
const router = express.Router();
require("dotenv").config();
//const dbSqlite = require("../db/conexion");
//const routerAdmin = require("../routes/admin");
const {getAll} = require("../db/conexion");

router.get("/", async (request, response)=> {
    const rows = await getAll("select * from integrantes",);
        console.log(rows);
        response.render("index",{
        integrantes:rows,
        materia:process.env.MATERIA,
        alumno:process.env.ALUMNO,
        repositorio:process.env.ENLACE_REPOSITORIO
    });
});



router.get("/integrante/:matricula", async (request, response, next) => {
    const matricula = request.params.matricula;
    //const media = await getAll("select * from media",);
    const tipoMedia = await getAll("select * from tipoMedia",);
    console.log("matri");
    if (matricula.includes(matricula)) {
        const integranteFilter = await getAll("select * from integrantes where matricula = ?",[matricula]);
        const integranteList = await getAll("select * from integrantes",);
        const mediaFilter =await getAll("select * from media where matricula = ?", [matricula]);
        response.render('integrante', {
            integrante: integranteFilter,
            integrantes:integranteList,
            tipoMedia:tipoMedia,
            media: mediaFilter,
            materia:process.env.MATERIA,
            alumno:process.env.ALUMNO,
            repositorio:process.env.ENLACE_REPOSITORIO
        });
    } else {
        next();
    }
});



router.get("/paginas/curso.html", async (request, response)=> {
    const rows = await getAll("select * from integrantes",);
    response.render("curso", {
        integrantes:rows,
        materia:process.env.MATERIA,
        alumno:process.env.ALUMNO,
        repositorio:process.env.ENLACE_REPOSITORIO
    });

});

router.get("/paginas/word_cloud.html", async (request, response)=> {
    const rows = await getAll("select * from integrantes",);
    response.render("word_cloud", {
        integrantes:rows,
        materia:process.env.MATERIA,
        alumno:process.env.ALUMNO,
        repositorio:process.env.ENLACE_REPOSITORIO
    });
});
/*
router.use((req, res) => {
    res.status(404).render('./partials/error_404');
});*/

//Exportamos el router
module.exports = router;